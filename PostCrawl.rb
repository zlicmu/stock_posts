require 'wombat'

puts "Started"
data = Wombat.crawl do
  base_url "http://guba.eastmoney.com"
  path "/list,002728.html"

  stock_name css: "div#stockheader span#stockname a"
  stock_code css: "div#stockheader span#stockheadercode a"
  # last_page css: "div.pager .pagernums"
  page_calculation xpath: "/html/.//div[@class='pager']/span[@class='pagernums']"

  posts({xpath: "/html/.//div[@class='articleh']"}, :iterator) do
    click_count xpath: "span[1]"
    # reply_count xpath: "span[2]"
    # title xpath: "span[3]/a"
    # link xpath: "span[3]/a/@href"
    # create_date xpath: "span[5]"
  end

  # click_counts({xpath: "/html/.//div[@class='articleh']/span[1][text()]"}, :list)
  # reply_counts({xpath: "/html/.//div[@class='articleh']/span[2][text()]"}, :list)
  # titles({xpath: "/html/.//div[@class='articleh']/span[3]/a[text()]"}, :list)
  
  # articles({css: "div#articlelistnew div.articleh a[title]"}, :follow) do
    # title css: "div#zwconttbt"
    # body css: "div#zwconbody div.stockcodec"
  # end
end

puts data

puts "End"