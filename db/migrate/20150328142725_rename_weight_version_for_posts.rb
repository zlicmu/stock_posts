class RenameWeightVersionForPosts < ActiveRecord::Migration
  def change
    rename_column :posts, :weight_version, :word_weight_version

    Post.where("word_weight_version != 0").update_all("word_weight_version = 1")
  end
end
