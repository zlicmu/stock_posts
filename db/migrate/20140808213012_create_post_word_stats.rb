class CreatePostWordStats < ActiveRecord::Migration
  def change
    create_table :post_word_stats, :id => false  do |t|
      t.integer :post_id
      t.integer :word_id
      t.integer :count

      t.timestamps
    end
  end
end
