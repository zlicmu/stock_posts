class AddIndexToStocks < ActiveRecord::Migration
  def change
    add_index(:stocks, [:stock_id])
  end
end
