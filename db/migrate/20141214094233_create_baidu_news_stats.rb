class CreateBaiduNewsStats < ActiveRecord::Migration
  def change
    create_table :baidu_news_stats do |t|
      t.integer :baidu_task_id
      t.integer :stock_id
      t.integer :news_count

      t.timestamps
    end

    add_index(:baidu_news_stats, [:baidu_task_id])
    add_index(:baidu_news_stats, [:stock_id])
    add_index(:baidu_news_stats, [:baidu_task_id, :stock_id], unique: true)
  end
end
