class AddWeightVersionToWords < ActiveRecord::Migration
  def change
    add_column :words, :weight_version, :integer, default: 1
  end
end
