class AddMoreWeightsToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :negative_weight, :decimal
    add_column :posts, :positive_weight, :decimal
  end
end
