class FixBaiduNewsStatsStockIdVarchar < ActiveRecord::Migration
  def change
    change_column(:baidu_news_stats, :stock_id, :string)
  end
end
