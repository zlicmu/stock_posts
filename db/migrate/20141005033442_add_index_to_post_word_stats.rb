class AddIndexToPostWordStats < ActiveRecord::Migration
  def change
    add_index(:post_word_stats, [:post_id, :word_id], unique: true)
  end
end
