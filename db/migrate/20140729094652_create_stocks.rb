class CreateStocks < ActiveRecord::Migration
  def change
    create_table :stocks, :id => false do |t|
      t.string :stock_id
      t.string :stock_name
      t.string :status
      t.integer :total_posts
      t.decimal :total_weight

      t.timestamps
    end
  end
end
