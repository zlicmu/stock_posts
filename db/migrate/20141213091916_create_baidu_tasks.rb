class CreateBaiduTasks < ActiveRecord::Migration
  def change
    create_table :baidu_tasks do |t|
      t.date :start_date 
      t.date :end_date
      t.string :status, limit: 10
      t.integer :total
      t.integer :processed
      t.integer :lock_version, default: 0, null: false

      t.timestamps
    end

    add_index(:baidu_tasks, [:start_date, :end_date], unique: true)
  end
end
