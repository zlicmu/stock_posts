class RemoveBaiduTaskUniqueIndex < ActiveRecord::Migration
  def change
    remove_index(:baidu_tasks, column: [:start_date, :end_date])
    add_index(:baidu_tasks, [:start_date, :end_date])
  end
end
