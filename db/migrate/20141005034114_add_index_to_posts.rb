class AddIndexToPosts < ActiveRecord::Migration
  def change
    add_index(:posts, [:stock_id])
  end
end
