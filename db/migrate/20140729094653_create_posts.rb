class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string 'path'
      t.string 'stock_id'
      t.string 'title'
      t.text 'content'
      t.integer 'click_count'
      t.timestamp 'post_created_at'
      t.decimal 'total_weight'

      t.timestamps
    end

    add_index :posts, [:path]
    add_index :posts, [:stock_id, :click_count]
    add_index :posts, [:stock_id, :post_created_at]
  end
end
