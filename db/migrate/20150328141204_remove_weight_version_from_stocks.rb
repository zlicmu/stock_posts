class RemoveWeightVersionFromStocks < ActiveRecord::Migration
  def change
    remove_column :stocks, :weight_version, :integer, default: 0
    remove_column :stocks, :total_weight, :decimal
  end
end
