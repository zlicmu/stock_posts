class AddVersionToWeight < ActiveRecord::Migration
  def change
    add_column :stocks, :weight_version, :integer, default: 0
    add_column :posts, :weight_version, :integer, default: 0

    add_index(:posts, [:stock_id, :weight_version])
    add_index(:post_word_stats, [:post_id])
    add_index(:post_word_stats, [:word_id])
  end
end
