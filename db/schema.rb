# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150328142725) do

  create_table "baidu_news_stats", force: true do |t|
    t.integer  "baidu_task_id"
    t.string   "stock_id"
    t.integer  "news_count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "baidu_news_stats", ["baidu_task_id", "stock_id"], name: "index_baidu_news_stats_on_baidu_task_id_and_stock_id", unique: true, using: :btree
  add_index "baidu_news_stats", ["baidu_task_id"], name: "index_baidu_news_stats_on_baidu_task_id", using: :btree
  add_index "baidu_news_stats", ["stock_id"], name: "index_baidu_news_stats_on_stock_id", using: :btree

  create_table "baidu_tasks", force: true do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.string   "status",       limit: 10
    t.integer  "total"
    t.integer  "processed"
    t.integer  "lock_version",            default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "baidu_tasks", ["start_date", "end_date"], name: "index_baidu_tasks_on_start_date_and_end_date", using: :btree

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "post_word_stats", id: false, force: true do |t|
    t.integer  "post_id"
    t.integer  "word_id"
    t.integer  "count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "post_word_stats", ["post_id", "word_id"], name: "index_post_word_stats_on_post_id_and_word_id", unique: true, using: :btree
  add_index "post_word_stats", ["post_id"], name: "index_post_word_stats_on_post_id", using: :btree
  add_index "post_word_stats", ["word_id"], name: "index_post_word_stats_on_word_id", using: :btree

  create_table "posts", force: true do |t|
    t.string   "path"
    t.string   "stock_id"
    t.string   "title"
    t.text     "content"
    t.integer  "click_count"
    t.datetime "post_created_at"
    t.decimal  "total_weight",        precision: 10, scale: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "word_weight_version",                          default: 0
    t.decimal  "negative_weight",     precision: 10, scale: 0
    t.decimal  "positive_weight",     precision: 10, scale: 0
    t.string   "status"
  end

  add_index "posts", ["path"], name: "index_posts_on_path", using: :btree
  add_index "posts", ["status"], name: "index_posts_on_status", using: :btree
  add_index "posts", ["stock_id", "click_count"], name: "index_posts_on_stock_id_and_click_count", using: :btree
  add_index "posts", ["stock_id", "post_created_at"], name: "index_posts_on_stock_id_and_post_created_at", using: :btree
  add_index "posts", ["stock_id", "word_weight_version"], name: "index_posts_on_stock_id_and_word_weight_version", using: :btree
  add_index "posts", ["stock_id"], name: "index_posts_on_stock_id", using: :btree

  create_table "stocks", id: false, force: true do |t|
    t.string   "stock_id"
    t.string   "stock_name"
    t.string   "status"
    t.integer  "total_posts"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "stocks", ["stock_id"], name: "index_stocks_on_stock_id", using: :btree

  create_table "words", force: true do |t|
    t.string   "word"
    t.decimal  "weight",         precision: 10, scale: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "weight_version",                          default: 1
  end

end
