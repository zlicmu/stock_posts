CREATE TABLE acquisitions (
  company_permalink VARCHAR(256)  
  , company_name  VARCHAR(256)  
  , company_category_list VARCHAR(256)  
  , company_market  VARCHAR(256)  
  , company_country_code  VARCHAR(3)  
  , company_state_code  VARCHAR(2)  
  , company_region  VARCHAR(256)  
  , company_city  VARCHAR(256)  
  , acquirer_permalink  VARCHAR(256)  
  , acquirer_name VARCHAR(256)  
  , acquirer_category_list  VARCHAR(256)  
  , acquirer_market VARCHAR(256)  
  , acquirer_country_code VARCHAR(3)
  , acquirer_state_code VARCHAR(2)
  , acquirer_region VARCHAR(256)
  , acquirer_city VARCHAR(256)
  , acquired_at DATE
  , price_amount  INT
  , price_currency_code VARCHAR(3)
)
;

CREATE INDEX acq_company_permalink ON acquisitions (company_permalink);
CREATE INDEX acq_company_market ON acquisitions (company_market);
CREATE INDEX acq_acquirer_permalink ON acquisitions (acquirer_permalink);
CREATE INDEX acq_acquirer_name ON acquisitions (acquirer_name);
CREATE INDEX acq_acquirer_market ON acquisitions (acquirer_market);

LOAD DATA INFILE '/home/webuser/crunchbase-data/acquisitions.csv'
INTO TABLE acquisitions
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
lines terminated by '\n'
ignore 1 lines
(company_permalink,company_name,company_category_list,company_market,company_country_code,company_state_code,company_region,company_city,acquirer_permalink,acquirer_name,acquirer_category_list,acquirer_market,acquirer_country_code,acquirer_state_code,acquirer_region,acquirer_city,acquired_at,price_amount,price_currency_code)
;


CREATE TABLE companies (
permalink VARCHAR(256)  
, name  VARCHAR(256)  
, homepage_url  VARCHAR(256)  
, category_list VARCHAR(256)  
, market VARCHAR(256)  
, funding_total_usd INT
, status  VARCHAR(256)  
, country_code  VARCHAR(3)  
, state_code  VARCHAR(2)  
, region  VARCHAR(256)  
, city  VARCHAR(256)  
, funding_rounds  INT
, founded_at  DATE
, first_funding_at  DATE
, last_funding_at DATE
, PRIMARY KEY (permalink, name)
)
;

CREATE INDEX companies_permalink ON companies (permalink);
CREATE INDEX companies_market ON companies (market);

LOAD DATA INFILE '/home/webuser/crunchbase-data/companies.csv'
INTO TABLE companies
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
lines terminated by '\n'
ignore 1 lines
;


CREATE TABLE investments (
company_permalink VARCHAR(256)  
, company_name  VARCHAR(256)  
, company_category_list VARCHAR(256)  
, company_market  VARCHAR(256)  
, company_country_code  VARCHAR(3)  
, company_state_code  VARCHAR(2)  
, company_region  VARCHAR(256)  
, company_city  VARCHAR(256)  
, investor_permalink  VARCHAR(256)  
, investor_name VARCHAR(256)  
, investor_category_list  VARCHAR(256)  
, investor_market VARCHAR(256)  
, investor_country_code VARCHAR(3)  
, investor_state_code VARCHAR(2)  
, investor_region VARCHAR(256)  
, investor_city VARCHAR(256)  
, funding_round_permalink VARCHAR(256)  
, funding_round_type  VARCHAR(256)  
, funding_round_code  CHAR(1)  
, funded_at DATE
, raised_amount_usd INT
)
;

CREATE INDEX investments_company_permalink ON investments (company_permalink);
--CREATE INDEX investments_company_name ON investments (company_name);
CREATE INDEX investments_company_market ON investments (compnay_market);
CREATE INDEX investments_investor_permalink ON investments (investor_permalink);
--CREATE INDEX investments_investor_name ON investments (investor_name);
CREATE INDEX investments_investor_market ON investments (investor_market);

LOAD DATA INFILE '/home/webuser/crunchbase-data/investments.csv'
INTO TABLE investments
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
lines terminated by '\n'
ignore 1 lines
;


CREATE TABLE rounds (
company_permalink VARCHAR(256)  
, company_name  VARCHAR(256)  
, company_category_list VARCHAR(256)  
, company_market  VARCHAR(256)  
, company_country_code  VARCHAR(3)  
, company_state_code  VARCHAR(2)  
, company_region  VARCHAR(256)  
, company_city  VARCHAR(256)  
, funding_round_permalink VARCHAR(256)  
, funding_round_type  VARCHAR(256)  
, funding_round_code  CHAR(1) 
, funded_at DATE
, raised_amount_usd INT
)
;

CREATE INDEX rounds_company_permalink ON rounds (company_permalink);

LOAD DATA INFILE '/home/webuser/crunchbase-data/rounds.csv'
INTO TABLE rounds
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
lines terminated by '\n'
ignore 1 lines
;
