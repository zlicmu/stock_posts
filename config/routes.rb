Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'stocks#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'
  get '/ping' => 'status#ping'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  resources :stocks do
    member do
      post 'crawl_posts'
      post 'update_weights'
    end

    collection do
      post 'crawl_posts_multiple'
      post 'update_weights_multiple'
    end

    resources :posts do
      collection do
        get 'chart'
      end
    end
  end

  resources :posts do
    collection do
      get 'download_monthly_summary', format: :csv
    end
  end

  resources :words

  resources :baidu_news, only: [:index, :create, :show, :destroy] do
    member do
      get 'download', format: :csv
    end
  end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
