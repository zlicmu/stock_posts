filename = '/tmp/monthly_export.csv'
File.open( filename, 'w' ) do |writer|
  writer.write Post.summary_csv_header.to_s
  
  Stock.where("status = 'UPDATED'").find_each do |stock|
    Post.summary_csv("month", [stock.stock_id], nil, -1).each do |summary|
      writer.write summary.to_s
    end
  end
end

filename = '/tmp/daily_export.csv'
File.open( filename, 'w' ) do |writer|
  writer.write Post.summary_csv_header.to_s
  
  Stock.where("status = 'UPDATED'").find_each do |stock|
    Post.summary_csv("day", [stock.stock_id], nil, -1).each do |summary|
      writer.write summary.to_s
    end
  end
end

BaiduTask.find_each do |bt|
  bt.rerun
end


Stock.where(["stock_id >= ?", '600694']).find_each do |stock|
  stock.crawl_posts
end

Stock.where(["status = ?", 'CRAWLED']).find_each do |stock|
  stock.update_word_analysis
end

Stock.where(["status = ? AND stock_id > 45", "UPDATED"]).find_each do |stock|
  stock.update_posts_create_time
end
