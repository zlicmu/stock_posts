class StatusController < ApplicationController

  def ping
    ActiveRecord::Base.establish_connection(
      Rails.configuration.database_configuration[Rails.env]
    )
    render layout: false
  end

end
