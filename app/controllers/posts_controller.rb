class PostsController < ApplicationController
  before_filter :find_stock

  def index
    if @stock
      @posts = @stock.posts.paginate(:page => params[:page])
    end
  end

  def chart
    if @stock
      stats = Post.posts_weight_time_series_by_stock_id(@stock.stock_id)
      @data = stats.map { |s| ts = s.post_dt.split("-"); [Time.utc(ts[0].to_i, ts[1].to_i, ts[2].to_i).to_i*1000, s.weight] }
    end
  end

  def download_monthly_summary
    respond_to do |format|
      format.csv { render_monthly_summary }
    end
  end

  private
  def find_stock
    @stock = Stock.find(params[:stock_id]) if params[:stock_id]
  end

  def set_file_headers
    file_name = "monthly_summary.csv"
    headers["Content-Type"] = "text/csv"
    headers["Content-Disposition"] = "attachment; filename=\"#{file_name}\""
  end

  def set_streaming_headers
    #nginx doc: Setting this to "no" will allow unbuffered responses suitable for Comet and HTTP streaming applications
    headers['X-Accel-Buffering'] = 'no'
    headers["Cache-Control"] ||= "no-cache"
    headers.delete("Content-Length")
  end

  def render_monthly_summary
    set_file_headers
    set_streaming_headers
    response.status = 200

    #setting the body to an enumerator, rails will iterate this enumerator
    self.response_body = monthly_summary_lines
  end

  def monthly_summary_lines
    Enumerator.new do |y|
      y << Post.summary_csv_header.to_s

      #ideally you'd validate the params, skipping here for brevity
      Post.monthly_summary_csv(params[:stock_ids], params[:year]).each { |summary| 
        y << summary.to_s
      }
    end
  end
end
