class WordsController < ApplicationController
  def index
    @words = Word.all
  end

  def new
    @word = Word.new
  end

  def create
    @word = Stock.new(word_params)
    if @word.save
      flash[:notice] = "Word created successfully."
      redirect_to :action => :index
    else
      render "new"
    end
  end

  private

  def word_params
    params.require(:word).permit(:word, :weight)
  end
end
