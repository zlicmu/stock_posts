class BaiduNewsController < ApplicationController
  def index
    paginated_baidu_tasks
    @baidu_task = BaiduTask.new
  end

  def create
    @baidu_task = BaiduTask.new(baidu_task_params)
    if @baidu_task.save
      @baidu_task.start
      flash[:notice] = "A new search is created"
      render action: :show
    else
      paginated_baidu_tasks
      flash[:error] = @baidu_task.errors.full_messages
      render :index
    end
  end

  def show
    unless baidu_task.nil?
      flash[:error] = "Record not found"
      redirect_to action: :index
    end
  end

  def download
    baidu_task
    respond_to do |format|
      format.csv { render_csv }
    end
  end

  def destroy
    if baidu_task
      if baidu_task.destroy
        flash[:notice] = "Baidu news search is deleted successfully"
      else
        flash[:error] = "Failed to delete"
      end
    else
      flash[:error] = "Record not found"
    end
  end

  private
  def baidu_task
    @baidu_task = BaiduTask.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    nil
  end

  def paginated_baidu_tasks
    @baidu_tasks = BaiduTask.all.order("updated_at DESC").paginate(:page => params[:page])
  end

  def baidu_task_params
    params.require(:baidu_task).permit(:start_date, :end_date)
  end

  def set_file_headers
    file_name = "BaiduNews_#{@baidu_task.start_date}-#{@baidu_task.end_date}.csv"
    headers["Content-Type"] = "text/csv"
    headers["Content-Disposition"] = "attachment; filename=\"#{file_name}\""
  end

  def set_streaming_headers
    #nginx doc: Setting this to "no" will allow unbuffered responses suitable for Comet and HTTP streaming applications
    headers['X-Accel-Buffering'] = 'no'
    headers["Cache-Control"] ||= "no-cache"
    headers.delete("Content-Length")
  end

  def render_csv
    set_file_headers
    set_streaming_headers
    response.status = 200

    #setting the body to an enumerator, rails will iterate this enumerator
    self.response_body = csv_lines
  end

  def csv_lines
    Enumerator.new do |y|
      y << BaiduNewsStat.csv_header.to_s

      #ideally you'd validate the params, skipping here for brevity
      @baidu_task.baidu_news_stats.each do |bns|
        y << bns.to_csv.to_s
      end
    end
  end
end
