class StocksController < ApplicationController
  def index
    @stocks = Stock.all.paginate(:page => params[:page])
  end

  def new
    @stock = Stock.new
  end

  def create
    failed_to_create = Stock.create_by_stock_ids(stock_params[:stock_ids], stock_params[:delete_old_records])
    if failed_to_create.blank?
      flash[:notice] = "Stocks created successfully."
      redirect_to :action => :index
    else
      flash[:error] = "Failed to create stocks: #{failed_to_create.join(", ")}"
      render "new"
    end
  end

  def crawl_posts_multiple
    if params[:all].present? and params[:all].downcase == 'true'
      Stock.transaction do
        Stock.find_each do |stock|
          set_status_and_start_job(stock, :crawl)
        end
      end
    elsif params[:stock_ids].present? and params[:stock_ids].class == Array
      Stock.transaction do
        Stock.where(:stock_id => params[:stock_ids]).find_each do |stock|
          set_status_and_start_job(stock, :crawl)
        end
      end
    end
    redirect_to :action => :index
  end

  def update_weights_multiple
    if params[:all].present? and params[:all].downcase == 'true'
      Stock.transaction do
        Stock.find_each do |stock|
          set_status_and_start_job(stock, :update_weights)
        end
      end
    elsif params[:stock_ids].present? and params[:stock_ids].class == Array
      Stock.transaction do
        Stock.where(:stock_id => params[:stock_ids]).find_each do |stock|
          set_status_and_start_job(stock, :update_weights)
        end
      end
    end
    redirect_to :action => :index
  end

  def crawl_posts
    @stock = Stock.find(params[:id])
    if @stock.idle?
      @stock.status = Stock::STATUS[:crawling]
      @stock.save
      @stock.crawl_posts
    else
      flash[:error] = "A task is running for #{@stock.stock_id}."
    end
    redirect_to :action => :index
  end

  def update_weights
    @stock = Stock.find(params[:id])
    if @stock.idle?
      @stock.status = Stock::STATUS[:updating]
      @stock.save
      @stock.update_word_analysis
    else
      flash[:error] = "A task is running for #{@stock.stock_id}."
    end
    redirect_to :action => :index
  end

  private

  def stock_params
    params.require(:stock).permit(:stock_ids, :delete_old_records)
  end

  def set_status_and_start_job(stock, job_name)
    if stock.idle?
      status = nil
      action = nil
      case job_name
      when :crawl
        status = Stock::STATUS[:crawling]
        action = :crawl_posts
      when :update_weights
        status = Stock::STATUS[:updating]
        action = :update_word_analysis
      end
    
      stock.status = status
      stock.save
      stock.send(action)
    end
  end
end
