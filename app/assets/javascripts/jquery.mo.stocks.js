/*
 * Mappedon jQuery Libraries - Stock
 * Auth: Eric Li
 * Date: October 27, 2014
 */

var MappedOn = MappedOn || {};

(function($) {
  MappedOn.Stocks = {
    _stockFormId: "stocks",
    _crawlPostsUrl: "/stocks/crawl_posts_multiple",
    _updateWeightsUrl: "/stocks/update_weights_multiple",

    _stockForm: function() {
      return $("form#"+this._stockFormId);
    },
    
    CrawlPosts: function() {
      var stockForm = this._stockForm();
      stockForm.attr("action", this._crawlPostsUrl);
      stockForm.submit();
    },
    UpdateWeights: function() {
      var stockForm = this._stockForm();
      stockForm.attr("action", this._updateWeightsUrl);
      stockForm.submit();
    }
  }
})(jQuery);