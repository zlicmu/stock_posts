class BaiduTask < ActiveRecord::Base
  self.per_page = 20

  has_many :baidu_news_stats, :dependent => :destroy
  
  STATUS = {
    :crawling => "CRAWLING",
    :done => "DONE"
  }

  validates :start_date, :end_date, presence: true, format: { with: /\d{4}-\d{2}-\d{2}/, message: "format should be yyyy-mm-dd" }
  validates_each :start_date, :end_date do |record, attr, value|
    record.errors.add(attr, 'date must be before current date') if value and value > Time.now
  end

  def start
    if updatable?
      Rails.logger.info "Update all stocks news on Baidu #{Time.now}: #{start_date} - #{end_date}"
      perform_search
    end
  end

  def rerun
    if idle?
      Rails.logger.info "Restart all stocks news on Baidu #{Time.now}: #{start_date} - #{end_date}"
      BaiduNewsStat.delete_all(baidu_task_id: self.id)
      perform_search
    end
  end

  handle_asynchronously :start
  handle_asynchronously :rerun

  def perform_search
    self.status = STATUS[:crawling] 
    self.total = Stock.has_stock_name.count
    self.processed = 0
    self.save!

    Stock.has_stock_name.find_each do |stock|
      bns = BaiduNewsStat.create!(baidu_task_id: self.id, stock_id: stock.id)
      self.processed += 1 if bns.fetch_news_stat
    end
    self.status = STATUS[:done]
    self.save!
  end

  def start_date_for_search
    start_date.to_time
  end

  def end_date_for_search
    end_date + 1.day - 1.second
  end

  def idle?
    ![STATUS[:crawling]].include?(self.status)
  end

  def done?
    self.status == STATUS[:done] 
  end

  def successful?
    !self.total.nil? and !self.processed.nil? and self.total <= self.processed
  end

  def updatable?
    idle? and !successful?
  end

  def downloadable?
    done? and successful?
  end
end