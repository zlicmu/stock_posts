require 'csv'

class Stock < ActiveRecord::Base
  self.primary_key = :stock_id
  self.per_page = 20

  has_many :posts, :foreign_key => :stock_id

  attr_accessor :max_possible_post_created_dt
  attr_reader :posts_path, :posts_path_no_html

  after_initialize :set_posts_path

  validates :stock_id, presence: true, uniqueness: true, numericality: { only_integer: true }

  scope :has_stock_name, -> { where("stock_name IS NOT NULL") }

  STATUS = {
    :crawling => "CRAWLING",
    :crawled => "CRAWLED",
    :updating => "UPDATING",
    :updated => "UPDATED",
    :update_failure => "UPDT FAIL",
    :time_crawling => "TIME CRAWLING",
    :time_updated => "TIME UPDT"
  }

  BASE = "http://guba.eastmoney.com"
  PER_PAGE = 1000

  def self.crawl_and_update_all
    Rails.logger.info "Crawl and update all stocks #{Time.now}"
    Stock.find_each do |stock|
      stock.crawl_and_update if stock.idle?
    end
  end

  def self.create_by_stock_ids(stock_ids, delete_old_records=false)
    ActiveRecord::Base.connection.execute("TRUNCATE stocks") if delete_old_records
    failed_to_create = []
    CSV.parse(stock_ids) do |row|
      stock_id = row.first.split('.').first
      stock = nil
      stock = Stock.find_by(stock_id: stock_id) unless delete_old_records
      stock = Stock.new(stock_id: stock_id) if stock.blank?
      unless stock.save
        Rails.logger.error "Stock ID #{stock_id}: #{stock.errors.full_messages.join(". ")}"
        failed_to_create << row
      end
    end
    failed_to_create
  end

  def stock_name_gbk
    stock_name_stripped.encode('gb2312', 'utf-8')
  end

  def stock_name_stripped
    self.stock_name[-1] == '吧' ? self.stock_name[0..-2] : self.stock_name if self.stock_name
  end

  def crawl_and_update
    crawl_posts_without_delay
    update_word_analysis_without_delay
  end

  def crawl_posts
    self.status = STATUS[:crawling]
    self.save!

    posts_path = self.posts_path
    Rails.logger.info "Start to crawl: #{BASE}/#{posts_path}"
    
    # Fetch exact post create date for the first page, since there will be recommended topics that not follow chronicle order
    first_page = posts_by_page(BASE, self.posts_path, true)

    page_strings = first_page["page_calculation"].split("|")
    posts_count = page_strings[-3].to_i
    posts_per_page = page_strings[-2].to_f
    pages_count = (posts_count / posts_per_page).ceil
    Rails.logger.info "Number of pages: #{pages_count}"

    #if self.stock_id != first_page["stock_code"]
    #  raise "Stock ID is incorrect: #{stock_id} vs #{first_page["stock_code"]}"
    #end
    self.stock_name = first_page["stock_name"]
    
    continue_to_crawl = save_posts(first_page["posts"], first_page["posts_detail"], (Time.now-1.year))
    current_page = 2
    while(current_page <= pages_count and continue_to_crawl) do
      posts_path_current_page = "#{self.posts_path_no_html}_#{current_page}.html"
      Rails.logger.info "URL: #{posts_path_current_page}"
      data = posts_by_page(BASE, posts_path_current_page, false)
      continue_to_crawl = save_posts(data["posts"])
      current_page += 1
    end

    self.status = STATUS[:crawled]
    self.total_posts = self.posts.size
    self.save!
    return true
  end

  def update_posts_create_time
    self.status = STATUS[:time_crawling]
    self.save!

    Rails.logger.info "Start to update posts create time for Stock: #{self.stock_id}"

    Stock.transaction do
      self.posts.find_each do |post|
        Rails.logger.info "Post create time update for: #{post.id}"
        data = post_by_page(BASE, "/#{post.path}")

        if data["post_created_at"]
          post.post_created_at = parse_post_create_time(data["post_created_at"])
          post.status = Post::STATUS[:post_create_time_updated]
        else
          Rails.logger.error "Failed to fetch Post Create Time for post: #{post.id}"
          post.post_created_at = nil
          post.status = Post::STATUS[:failed_to_fetch_post_create_time]
        end

        Rails.logger.error "Failed to SAVE post #{post.id}: #{post.errors.full_messages}" unless post.save
      end
    end

    self.status = STATUS[:time_updated]
    self.save!

    Rails.logger.info "Finish to update posts create time for Stock: #{self.stock_id}"
  end

  def crawl_posts_details
    self.status = STATUS[:crawling]
    self.save!

    posts_path = self.posts_path
    Rails.logger.info "Start to crawl: #{BASE}/#{posts_path}"
    first_page = Wombat.crawl do
      base_url BASE
      path posts_path

      stock_name css: "div#stockheader span#stockname a"
      stock_code css: "div#stockheader span#stockheadercode a"
      page_calculation xpath: "/html/.//div[@class='pager']/span[@class='pagernums']/@data-pager"

      posts({xpath: "/html/.//div[@class='articleh']"}, :iterator) do
        click_count xpath: "span[1]"
        reply_count xpath: "span[2]"
        # title xpath: "span[3]/a"
        path xpath: "span[3]/a/@href"
      end

      posts_detail({css: "div#articlelistnew div.articleh a[title]"}, :follow) do
        title css: "div#zwconttbt"
        content css: "div#zwconbody div.stockcodec"
        post_created_at css: "div#zwconbot div#zwconbotl" do |time|
          time.blank? ? nil : time.split(/发表时间：/).last
        end
      end
    end

    page_strings = first_page["page_calculation"].split("|")
    posts_count = page_strings[-3].to_i
    posts_per_page = page_strings[-2].to_f
    pages_count = (posts_count / posts_per_page).ceil
    Rails.logger.info "Number of pages: #{pages_count}"

    #if self.stock_id != first_page["stock_code"]
    #  raise "Stock ID is incorrect: #{stock_id} vs #{first_page["stock_code"]}"
    #end
    self.stock_name = first_page["stock_name"]
    
    save_posts_detail(first_page["posts"], first_page["posts_detail"])

    (2..pages_count).each do |i|
      posts_path_i = "#{self.posts_path_no_html}_#{i}.html"
      Rails.logger.info "URL: #{posts_path_i}"
      data = Wombat.crawl do
        base_url BASE
        path posts_path_i

        posts({xpath: "/html/.//div[@class='articleh']"}, :iterator) do
          click_count xpath: "span[1]"
          reply_count xpath: "span[2]"
          path xpath: "span[3]/a/@href"
        end

        posts_detail({css: "div#articlelistnew div.articleh a[title]"}, :follow) do
          title css: "div#zwconttbt"
          content css: "div#zwconbody div.stockcodec"
          post_created_at css: "div#zwconbot div#zwconbotl" do |time|
            time.blank? ? nil : time.split(/发表时间：/).last
          end
        end
      end
      save_posts_detail(data["posts"], data["posts_detail"])
    end

    self.status = STATUS[:crawled]
    self.total_posts = self.posts.size
    self.save!
    return true
  end

  def save_posts(posts, posts_detail=[], update_existing_posts_after=(Time.now-30.days))
    continue_to_crawl = true
    Post.transaction do
      posts.each_index do |i|
        p = posts[i]
        d = posts_detail[i]

        # Remove posts that are not specific to this stock (usually the first 4 posts)
        # next if p["path"].split(",")[1].to_i != self.stock_id[0..6].to_i

        content = nil
        time_parsed = nil
        if d.present?
          content = d["content"].nil? ? nil : d["content"].squish
          time_parsed = d["post_created_at"].blank? ? nil : parse_post_create_time(d["post_created_at"])
        elsif !p["post_created_dt"].blank?
          time_parsed = Time.strptime("#{self.max_possible_post_created_dt.year}-#{p['post_created_dt']}+0800", '%Y-%m-%d%z')
          # Previous Year: sometime Guba sort by time have some posts not in correct order
          if (time_parsed - self.max_possible_post_created_dt) > 5.days
            time_parsed = Time.strptime("#{self.max_possible_post_created_dt.year-1}-#{p['post_created_dt']}+0800", '%Y-%m-%d%z')
          end
        end

        post = Post.find_by(path: p["path"], stock_id: self.stock_id)
        if post.present?
          # Some stocks' forum are really active. Page number changes while crawling. 
          # For example, Post A shows up on Page n, however, it might show up on Page n+1 again since new posts push it down to the next page.
          # We only skip updating posts if they are created 1 day and older. 
          if time_parsed.present? and time_parsed < update_existing_posts_after and (Time.now - post.created_at) > 1.day
            Rails.logger.debug "Do not update posts older than certain days, POST ID: #{post.id}"
            continue_to_crawl = false
            break
          end
        else
          post = Post.new
        end

        post.path = p["path"]
        post.stock_id = self.stock_id
        post.title = p["title"]
        post.click_count = p["click_count"]
        post.post_created_at = time_parsed
        post.content = content

        begin
          post.save!
        rescue Exception => e
          Rails.logger.error "Post not saved properly: #{post.path}, #{e}"
        end
        
        self.max_possible_post_created_dt = time_parsed if time_parsed.present?
      end
    end

    continue_to_crawl
  end

  def update_word_analysis
    self.status = STATUS[:updating]
    self.save!

    words = Word.all
    weight_version = words.first.weight_version

    counter = 0
    # Update word analysis for new Posts (New posts have weight version 0, will be selected automatically) or Posts with older weight version
    while(!(posts = self.posts.without_content.where("word_weight_version != ?", weight_version).limit(PER_PAGE)).blank?) do
      PostWordStat.delete_all(:post_id => posts.map {|p| p.id})
      Post.transaction do
        posts.each do |post|
          negative_weight_for_post = 0
          positive_weight_for_post = 0
          total_weight_for_post = 0
          words.each do |word|
            # word_count = post.title.blank? ? 0 : post.title.scan(word.word).size
            # Count only once for each word, even if it occurs multiple times
            word_count = post.title.blank? ? 0 : (post.title.include?(word.word) ? 1 : 0)
            if word_count > 0
              pws = PostWordStat.new(:post_id => post.id, :word_id => word.id)
              pws.count = word_count
              pws.save!
              if word.weight > 0
                positive_weight_for_post += word.weight * word_count
              elsif word.weight < 0
                negative_weight_for_post += word.weight * word_count
              end
              total_weight_for_post += word.weight * word_count
            end
          end
          post.negative_weight = negative_weight_for_post
          post.positive_weight = positive_weight_for_post
          post.total_weight = total_weight_for_post
          post.word_weight_version = weight_version
          post.save!
        end
      end

      counter += 1
      Rails.logger.info "Updating posts weight: loop ##{counter}"
    end

    self.status = STATUS[:updated]
    self.save!
    return true
  rescue Exception => e
    Rails.logger.error e
    self.status = STATUS[:update_failure]
    self.save!
    return false
  end

  handle_asynchronously :crawl_and_update
  handle_asynchronously :crawl_posts
  handle_asynchronously :crawl_posts_details
  handle_asynchronously :update_word_analysis
  handle_asynchronously :update_posts_create_time

  def idle?
    ![STATUS[:crawling], STATUS[:updating], STATUS[:time_crawling]].include?(self.status)
  end

  private
  def set_posts_path
    @posts_path = "/list,#{stock_id},f.html"
    @posts_path_no_html = "/list,#{stock_id},f"
    @max_possible_post_created_dt = Time.now
  end

  def posts_by_page(base_url, posts_path, fetch_posts_detail=false)
    Wombat.crawl do
      base_url base_url
      path posts_path

      stock_name css: "div#stockheader span#stockname a"
      stock_code css: "div#stockheader span#stockheadercode a"
      page_calculation xpath: "/html/.//div[@class='pager']/span[@class='pagernums']/@data-pager"

      posts({xpath: "/html/.//div[@class='articleh']"}, :iterator) do
        click_count xpath: "span[1]"
        reply_count xpath: "span[2]"
        title xpath: "span[3]/a"
        path xpath: "span[3]/a/@href" do |p|
          (p[0] == '/') ? p[1..-1] : p
        end
        post_created_dt xpath: "span[5]"
      end

      posts_detail({css: "div#articlelistnew div.articleh a[title]"}, :follow) do
        # title css: "div#zwconttbt"
        content css: "div#zwcontent div#zwconbody div.stockcodec"
        post_created_at css: "div#zwcontent div#zwcontt div.zwfbtime" do |time|
          Rails.logger.info("post_created_at: #{time}")
          if time.blank?
            nil
          else
            time_split = time.split(/发表于 /).last.split(" ")
            time_split[0] + " " + time_split[1]
          end
        end
      end if fetch_posts_detail
    end
  end

  def post_by_page(base_url, post_path, fetch_post_content=false)
    Wombat.crawl do
      base_url base_url
      path post_path

      content css: "div#zwcontent div#zwconbody div.stockcodec" if fetch_post_content
      post_created_at css: "div#zwcontent div#zwcontt div.zwfbtime" do |time|
        Rails.logger.info("post_created_at: #{time}")
        if time.blank?
          nil
        else
          time_split = time.split(/发表于 /).last.split(" ")
          time_split[0] + " " + time_split[1]
        end
      end
    end
  end

  def parse_post_create_time(time_str)
    Time.strptime(time_str+'+0800', '%Y-%m-%d %H:%M:%S%z')
  rescue ArgumentError
    nil
  end
end
