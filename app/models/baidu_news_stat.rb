require 'csv'

class BaiduNewsStat < ActiveRecord::Base
  belongs_to :baidu_task
  belongs_to :stock

  delegate :start_date_for_search, :end_date_for_search, to: :baidu_task
  delegate :stock_name, :stock_name_gbk, :stock_name_stripped, to: :stock

  validates_presence_of :baidu_task_id, :stock_id

  def self.csv_header
    CSV::Row.new([:stock_id, :stock_name, :news_count], ['代码', '名称', '发帖数量'], true)
  end

  def fetch_news_stat
    response = baidu_connection.get do |req|
      req.url '/ns'
      req.params = baidu_params
    end
    doc = Nokogiri::HTML(response.body)
    match_data = doc.css('div#header_top_bar span.nums').text.scan(/[\d,]+/)
    if match_data.size == 1
      self.news_count = match_data.first.gsub(',','').to_i
      self.save!
      return true
    elsif match_data.size == 0
      Rails.logger.error("No news count data found for stock: #{self.id}")
      return false
    else
      Rails.logger.error("Multiple news counts found for stock: #{self.id}")
      return false
    end
  end

  def baidu_connection
    @baidu_connection ||= Faraday.new(url: 'http://news.baidu.com/ns') do |faraday|
      faraday.request :url_encoded
      faraday.response :logger
      faraday.adapter Faraday.default_adapter
    end
  end

  def baidu_params
    {
      from: 'news',
      cl: 2,
      bt: start_date_for_search.to_i,
      y0: start_date_for_search.year,
      m0: start_date_for_search.month,
      d0: start_date_for_search.day,
      y1: end_date_for_search.year,
      m1: end_date_for_search.month,
      d1: end_date_for_search.day,
      et: end_date_for_search.to_i,
      q1: self.stock.stock_name_gbk,
      submit: '百度一下'.encode('gb2312', 'utf-8'),
      q3: nil,
      q4: nil,
      s: 2,
      begin_date: start_date_for_search.strftime('%Y-%m-%d'),
      end_date: end_date_for_search.strftime('%Y-%m-%d'),
      tn: 'newstitledy',
      ct: 0,
      rn: 20,
      q6: nil
    }
  end

  def to_csv
    CSV::Row.new([:stock_id, :stock_name, :news_count], [self.stock_id, self.stock_name_stripped, self.news_count])
  end
end
