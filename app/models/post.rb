require 'csv'

class Post < ActiveRecord::Base
  MIN_CLICK_COUNT = 1000
  self.per_page = 20

  STATUS = {
    :crawled => 'CRAWLED',
    :failed_to_fetch_post_create_time => 'PCTFF',
    :post_create_time_updated => 'PCTDONE'
  }

  scope :without_content, -> {select("id, path, stock_id, title, click_count, post_created_at, total_weight, created_at, updated_at")}

  def self.posts_summary_by_stock_id(stock_id)
    find_by_sql(["
      SELECT SUM(positive) AS positive, SUM(negative) AS negative, SUM(total_weight) AS total, post_dt
      FROM
      (
      SELECT
        IF(total_weight > 0, total_weight, 0) AS positive,
        IF(total_weight < 0, total_weight, 0) AS negative,
        total_weight,
        DATE_FORMAT(post_created_at, '%Y-%m-%d') AS post_dt
      FROM posts
      WHERE stock_id = ?
        AND click_count > ?
      ) AS a
      GROUP BY post_dt
      ORDER BY post_dt
    ", stock_id, MIN_CLICK_COUNT])
  end

  def self.posts_weight_time_series_by_stock_id(stock_id)
    find_by_sql(["
      SELECT SUM(total_weight) AS weight, DATE_FORMAT(post_created_at, '%Y-%m-%d') AS post_dt
      FROM posts
      WHERE stock_id = ?
        AND click_count > ?
        AND total_weight IS NOT NULL
        AND post_created_at IS NOT NULL
      GROUP BY post_dt
      ORDER BY post_dt
    ", stock_id, MIN_CLICK_COUNT])
  end

  def self.summary_csv_header
    CSV::Row.new(
      [:stock_id, :stock_name, :post_dt, :posts_count, :positive_weight, :negative_weight, :total_weight, :total_posts_weight, 
        :adjusted_positive_weight, :adjusted_negative_weight, :adjusted_total_weight, :adjusted_total_posts_weight, :total_posts_count], 
      ['股票代码', '股票名称', '时间段', '帖子数', '情感正值', '情感负值', '情感和值', '全年总情感值', '情感正值修正', '情感负值修正', '情感和值修正', '全年总情感值修正', '全年总帖子数'], 
      true)
  end

  def self.summary_csv(group_by="month", stock_ids=[], year=nil, min_click_count=MIN_CLICK_COUNT)
    date_format = case group_by
    when "month"
      "%Y-%m"
    when "day"
      "%Y-%m-%d"
    end

    summary = unscoped.select("posts.stock_id, stocks.stock_name, COUNT(1) AS posts_count, 
      SUM(posts.negative_weight) AS negative_weight, 
      SUM(posts.positive_weight) AS positive_weight, 
      SUM(IF(posts.total_weight < 0, -1, 0)) AS adjusted_negative_weight,
      SUM(IF(posts.total_weight > 0, 1, 0)) AS adjusted_positive_weight,
      SUM(posts.total_weight) AS total_weight, 
      DATE_FORMAT(posts.post_created_at, '#{date_format}') post_dt").
    joins("INNER JOIN stocks ON stocks.stock_id = posts.stock_id").
    where("posts.click_count > ? AND posts.post_created_at IS NOT NULL", min_click_count).
    where(stock_ids.blank? ? "" : ["posts.stock_id IN (?)", stock_ids]).
    where(year.nil? ? "" : ["posts.post_created_at BETWEEN ? AND ?", "#{year.to_i}-01-01", "#{year.to_i}-12-31"]).
    group("posts.stock_id, post_dt").
    order("posts.stock_id, post_dt")

    summary_hash = {}
    summary.each do |s|
      summary_hash[s.stock_id] ||= {}
      summary_hash[s.stock_id]["total_posts_count"] ||= {}
      summary_hash[s.stock_id]["total_posts_weight"] ||= {}
      summary_hash[s.stock_id]["adjusted_total_posts_weight"] ||= {}

      summary_hash[s.stock_id]["total_posts_count"][s.post_dt[0..3]] ||= 0
      summary_hash[s.stock_id]["total_posts_weight"][s.post_dt[0..3]] ||= 0
      summary_hash[s.stock_id]["adjusted_total_posts_weight"][s.post_dt[0..3]] ||= 0

      summary_hash[s.stock_id]["data"] ||= []
      summary_hash[s.stock_id]["total_posts_count"][s.post_dt[0..3]] += s.posts_count || 0
      summary_hash[s.stock_id]["total_posts_weight"][s.post_dt[0..3]] += s.total_weight || 0
      summary_hash[s.stock_id]["adjusted_total_posts_weight"][s.post_dt[0..3]] += s.adjusted_positive_weight+s.adjusted_negative_weight || 0

      summary_hash[s.stock_id]["data"] << s
    end
    summary_csv = []
    summary_hash.each_key do |stock_id|
      summary_hash[stock_id]["data"].each do |d|
        summary_csv << CSV::Row.new(
          [:stock_id, :stock_name, :post_dt, :posts_count, :positive_weight, :negative_weight, :total_weight, :total_posts_weight, 
            :adjusted_positive_weight, :adjusted_negative_weight, :adjusted_total_weight, 
            :adjusted_total_posts_weight, :total_posts_count
          ], 
          [d.stock_id, d.stock_name, d.post_dt, d.posts_count, d.positive_weight, d.negative_weight, d.total_weight, summary_hash[stock_id]["total_posts_weight"][d.post_dt[0..3]],
            d.adjusted_positive_weight, d.adjusted_negative_weight, d.adjusted_positive_weight+d.adjusted_negative_weight, 
            summary_hash[stock_id]["adjusted_total_posts_weight"][d.post_dt[0..3]], summary_hash[stock_id]["total_posts_count"][d.post_dt[0..3]]
          ]
        )
      end
    end

    summary_csv
  end
end